from django.conf.urls import patterns, include, url
from rest_framework import viewsets, routers
from CentralAppManager.models import App


class AppViewSet(viewsets.ModelViewSet):
    queryset = App.objects.all()
    model = App

router = routers.DefaultRouter()
router.register(r'apps', AppViewSet)

urlpatterns = patterns('',
    url(r'^api/', include(router.urls)),
)