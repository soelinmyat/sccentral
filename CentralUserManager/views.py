import json

from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.contrib.auth import authenticate

from CentralUserManager.models import Student

@csrf_exempt
def check_user_credentials(request):
    if request.method == 'POST':

        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=username, password=password)
        student = Student.objects.get(user=user)
        student.set_passkey()
        student.save

        if user:

            if user.is_active:
                return HttpResponse(json.dumps({'message': 'success', 'passkey': student.passkey}), content_type="application/json")
            else:
                return HttpResponse(json.dumps({'message': 'fail'}), content_type="application/json")
        else:
            return HttpResponse(json.dumps({'message': 'fail'}), content_type="application/json")

    else:
        return HttpResponse(json.dumps({'message': 'fail'}), content_type="application/json")
