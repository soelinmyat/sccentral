import json
from datetime import datetime

from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse

from CentralHubManager.models import Hub, Status


#@require_http_methods(["POST"])
@csrf_exempt
def status(request):

    if request.method == 'POST':

        hub_id = request.POST['hub_id']
        ip = request.POST['ip']

        hub = Hub.objects.get(hub_id=hub_id)
        new_status = Status(ip=ip, hub=hub, added=datetime.now())

        new_status.save()

        return HttpResponse(json.dumps({'message': 'success'}), content_type="application/json")
    else:
        return HttpResponse(json.dumps({'message': 'error'}), content_type="application/json")
