from django.conf.urls import patterns, include, url
from rest_framework import viewsets, routers

from CentralUserManager import views
from CentralUserManager.models import Student

class StudentViewSet(viewsets.ModelViewSet):
    queryset = Student.objects.all()
    model = Student

router = routers.DefaultRouter()
router.register(r'', StudentViewSet)

urlpatterns = patterns('',
    url(r'^check_info/$', views.check_user_credentials, name='check info'),
    url(r'^api/', include(router.urls)),
)