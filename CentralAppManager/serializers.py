from rest_framework import serializers
from CentralAppManager.models import App


class AppSerializer(serializers.ModelSerializer):

    class Meta:
        model = App
        fields = ('id', 'package_name', 'desc', 'prefix', 'version')


class ReversedAppSerializer(serializers.ModelSerializer):

    class Meta:
        model = App
        fields = ('id', 'package_name', 'desc', 'prefix', 'version')