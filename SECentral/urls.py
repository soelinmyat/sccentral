from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'SECentral.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    # url(r'^hubs/', include('CentralHubManager.urls')),
    url(r'^', include('CentralInstructorPortal.urls')),
    url(r'^apps/', include('CentralAppManager.urls')),
    url(r'^hubs/', include('CentralHubManager.urls')),
    url(r'^users/', include('CentralUserManager.urls')),
)
