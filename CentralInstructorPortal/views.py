import json

from django.shortcuts import render, redirect
from django.contrib.auth import  authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt

from CentralHubManager.models import Hub


def user_login(request):

    context = RequestContext(request)

    if request.method == 'POST':

        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=username, password=password)

        if user:

            if user.is_active:
                login(request, user)
                return redirect('/')
            else:
                return HttpResponse("Your account is disabled.")
        else:
            return HttpResponse("Invalid login details supplied.")

    else:
        return render(request, 'CentralInstructorPortal/login.html', {})

def user_logout(request):
    logout(request)
    return render(request, 'CentralInstructorPortal/login.html', {})


@login_required(login_url='login/')
def index(request):
    hubs = Hub.objects.filter(user=request.user)
    return render(request, 'CentralInstructorPortal/index.html', {'hubs': hubs})
