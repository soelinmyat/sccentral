from django.conf.urls import patterns, include, url

from CentralHubManager import views

urlpatterns = patterns('',
    url(r'^status/$', views.status, name='status'),
)
