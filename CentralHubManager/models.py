from random import choice
from string import digits, letters

from django.db import models
from django.contrib.auth.models import User


def random_string(length):
    s = ''
    for i in range(length):
        s += choice(digits+letters)

    return s


class Hub(models.Model):
    hub_id = models.CharField(max_length=32, blank=True, default='')
    hub_key = models.CharField(max_length=128, blank=True, default='')
    user = models.ForeignKey(User)

    def __str__(self):
        return self.user.username + " - " + str(self.id)

    def save(self, force_insert=False, force_update=False):
        if self.hub_id == "":
            s = random_string(32)
            while Hub.objects.filter(hub_id=s).count():
                s = random_string(32)
            self.hub_id = s

        if self.hub_key == "":
            s = random_string(32)
            while Hub.objects.filter(hub_key=s).count():
                s = random_string(32)
            self.hub_key = s

        super(Hub, self).save(force_insert, force_update)

    def get_latest(self):
        return self.status_set.all().order_by('-added')[:5]


class Status(models.Model):
    hub = models.ForeignKey(Hub)
    ip = models.CharField(max_length=64)
    added = models.DateTimeField(auto_created=True)
