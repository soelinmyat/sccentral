from random import choice
from string import digits, letters

from django.db import models
from django.contrib.auth.models import User


def random_string(length):
    s = ''
    for i in range(length):
        s += choice(digits+letters)

    return s


class Student(models.Model):
    user = models.OneToOneField(User)
    username = models.CharField(max_length=64,blank=True)
    passkey = models.CharField(max_length=32,blank=True, default="")

    def save(self, force_insert=False, force_update=False):
        self.username = self.user.username
        if self.passkey == "":
            self.passkey = random_string(32)
        super(Student, self).save(force_insert, force_update)

    def set_passkey(self):
        self.passkey = random_string(32)


    def __str__(self):
        return self.username
