from django.contrib import admin
from CentralHubManager.models import Hub, Status


class StatusInline(admin.TabularInline):
    model = Status
    readonly_fields = ('ip', 'added')

    actions = None

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def save_model(selfself, request, obj, form, change):
        pass


class HubAdmin(admin.ModelAdmin):
    model = Hub
    readonly_fields = ('hub_id', 'hub_key')
    inlines = [StatusInline]


admin.site.register(Hub, HubAdmin)
