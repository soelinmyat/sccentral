from django.db import models


class App(models.Model):
    name = models.CharField(max_length=64)
    package_name = models.CharField(max_length=32)
    desc = models.TextField()
    prefix = models.CharField(max_length=64)
    version = models.CharField(max_length=16)

    def __unicode__(self):
        return self.name



